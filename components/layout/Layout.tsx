import NavLink from 'components/ui/NavLink/NavLink'
import Head from 'next/head'
import Link from 'next/link'
import React, { useState } from 'react'
import { BiUpArrow } from 'react-icons/bi'

interface LayoutProps {
    meta: {
        title: string
        description: string
        url: string
    }
    children: React.ReactNode
}

const Layout = ({meta, ...props}: LayoutProps) => {
    const [isSidebarOpen, setIsSidebarOpen] = useState(false)

    const scrollToTop = () => {
        const isSmoothScrollSupported = 'scrollBehavior' in document.documentElement.style
        
        if (isSmoothScrollSupported) {
            document.documentElement.scrollTo({
                left: 0,
                top: 0,
                behavior: "smooth",
            })
        } else {
            document.documentElement.scrollTop = 0
        }
    }

    return (
        <>
            <Head>
                <meta charSet="UTF-8" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>{meta.title}</title>
                <meta name="title" content={meta.title} />
                <meta name="description" content={meta.description} />
                <meta property="og:site_name" content="Female Daily" />
                <meta property="og:url" content="femaledaily.com" />
                <meta property="og:title" content={meta.title} />
                <meta property="og:description" content={meta.description} />
                <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon" />
            
                <link rel="canonical" href={meta.url} />
            </Head>
            <nav className='fixed top-0 left-0 right-0 h-[64px] md:h-[80px] shadow w-full bg-zinc-900 z-20'>
                <div className="container px-5 h-full flex items-center relative">
                    <Link href={`/`}>
                        <a className='md:hidden mx-auto w-[80px] h-[35px]'>
                            <img src="https://static-mh.content.disney.io/starwars/assets/navigation/sw_logo_stacked-336c62367939.png" alt="star wars logo" width={100} height={48} className="w-[80px] h-[35px] cursor-pointer" />
                        </a>
                    </Link>
                    <div className="hidden absolute left-1/2 top-1/2 md:flex items-center justify-center transform -translate-x-1/2 -translate-y-1/2">
                        <NavLink href="/people" text='People' />
                        <NavLink href="/planets" text='Planets' />
                        <NavLink href="/films" text='Films' />
                        <Link href={`/`}>
                            <a className="mx-10 w-[100px] h-[44px]">
                                <img src="https://static-mh.content.disney.io/starwars/assets/navigation/sw_logo_stacked-336c62367939.png" alt="star wars logo" width={100} height={48} className="w-[100px] h-[44px] cursor-pointer" />
                            </a>
                        </Link>
                        <NavLink href="/species" text='Species' />
                        <NavLink href="/vehicles" text='Vehicles' />
                        <NavLink href="/starships" text='Starships' />
                    </div>
                    <div className="md:hidden fixed right-0 top-0 mr-5 mt-5">
                        <button
                            className={`hamburger hamburger--slider ${isSidebarOpen ? 'is-active' : ''}`}
                            onClick={() => setIsSidebarOpen(prev => !prev)}
                            style={{ padding: '0' }}
                            type="button"
                        >
                            <span className="hamburger-box">
                                <span className="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </nav>
            <div className={`z-10 md:hidden fixed left-0 right-0 top-[64px] w-full bg-zinc-900 px-4 py-3 flex flex-col transform transform-transition ${isSidebarOpen ? 'translate-y-0' : '-translate-y-full'}`}>
                <NavLink href="/people" text='People' />
                <NavLink href="/planets" text='Planets' />
                <NavLink href="/films" text='Films' />
                <NavLink href="/species" text='Species' />
                <NavLink href="/vehicles" text='Vehicles' />
                <NavLink href="/starships" text='Starships' />
            </div>
            <div className="w-full pt-[80px] min-h-full bg-zinc-800">
                { props.children }
            </div>
            <button onClick={scrollToTop} className="fixed right-0 bottom-0 mr-5 mb-5 bg-zinc-900 color-transition hover:bg-zinc-700 w-10 h-10 rounded flex items-center justify-center cursor-pointer">
                <BiUpArrow color='white' size={24} />
            </button>
        </>
    )
}

export default Layout
