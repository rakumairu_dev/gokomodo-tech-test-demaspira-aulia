import React from 'react'


const SkeletonCard = () => {
    return (
        <div className="rounded-lg px-3 py-4 shadow-lg w-full bg-zinc-700">
            <div className="w-full h-5 loading rounded mb-4" />
            <div className="w-3/5 h-4 loading rounded mb-4" />
            <div className="w-2/5 h-4 loading rounded mb-4" />
            <div className="w-4/5 h-4 loading rounded" />
        </div>
    )
}

export default SkeletonCard
