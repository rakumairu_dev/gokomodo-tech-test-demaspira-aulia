import React from 'react'
import { IoCloseOutline, IoSearch } from 'react-icons/io5'

interface SearchProps {
    value: string
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
    isLoading: boolean
    onClear: () => void
}

const Search = (props: SearchProps) => {
    return (
        <div className="rounded border border-gray-200 flex items-center pr-3 focus-within:border-primary color-transition">
            <input
                type="text"
                placeholder='Search by name'
                className='bg-transparent py-1 px-3 text-gray-200'
                value={props.value}
                name="search"
                onChange={props.onChange}
            />
            {
                props.value.length === 0 ?
                    <IoSearch size={20} className="text-gray-200" />
                    :
                    <button onClick={props.onClear} disabled={props.isLoading}>
                        <IoCloseOutline size={20} className={`text-gray-200 ${props.isLoading ? 'opacity-50' : ''}`} />
                    </button>
            }
        </div>
    )
}

export default Search
