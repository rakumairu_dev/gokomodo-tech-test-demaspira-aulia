import Link from 'next/link'
import React from 'react'

interface NavLinkProps {
    href: string
    text: string
}

const NavLink = (props: NavLinkProps) => {
    return (
        <Link href={props.href}>
            <a className='text-white hover:text-primary font-bold text-sm color-transition uppercase cursor-pointer tracking-wide py-2 px-3'>
                {props.text}
            </a>
        </Link>
    )
}

export default NavLink
