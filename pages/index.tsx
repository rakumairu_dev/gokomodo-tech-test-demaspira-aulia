import React from "react"
import Layout from "components/layout/Layout"
import Link from "next/link"

const Home = () => {
    return (
        <Layout
            meta={{
                title: 'Star Wars API - Find everything about star wars here',
                description: 'Find everything about star wars here',
                url: '/',
            }}
        >
            <div className="w-full container py-10 px-5 md:px-40">
                <h1 className='text-xl text-gray-200 font-semibold mb-6 md:mb-8 py-1 pl-3 border-b-2 border-t-2 border-gray-500 uppercase'>
                    Content
                </h1>
                <div className="grid grid-cols-2 md:grid-cols-6 gap-5">
                    {
                        content.map((cnt, index) =>
                            <Link href={cnt.url} key={index}>
                                <div className="rounded-lg bg-zinc-700 overflow-hidden cursor-pointer shadow-transition md:hover:shadow-primary">
                                    <div className="w-full pt-[100%] relative">
                                        <img src={`/images${cnt.url}.jpg`} alt={cnt.label} className="absolute inset-0 w-full h-full object-cover object-center opacity-100 opacity-transition hover:opacity-70" />
                                    </div>
                                    <div className="border-t-2 border-primary px-3 pt-2 pb-4">
                                        <Link href={cnt.url}>
                                            <a className="text-white font-semibold uppercase tracking-wide color-transition md:hover:text-primary cursor-pointer mb-1.5">
                                                {cnt.label}
                                            </a>
                                        </Link>
                                        <p className="text-sm font-medium text-gray-400 leading-tight">
                                            {cnt.sub}
                                        </p>
                                    </div>
                                </div>
                            </Link>
                        )
                    }
                </div>
            </div>
        </Layout>
    )
}

const content = [
    {
        url: '/people',
        label: 'People',
        sub: 'You can see everyone in star wars universe here',
    },
    {
        url: '/planets',
        label: 'Planets',
        sub: "It's not only tatooine you know?",
    },
    {
        url: '/films',
        label: 'Films',
        sub: 'The original trilogy is still the best',
    },
    {
        url: '/species',
        label: 'Species',
        sub: "There's a lot of alien species in star wars",
    },
    {
        url: '/vehicles',
        label: 'Vehicles',
        sub: 'One of those hovercraft',
    },
    {
        url: '/starships',
        label: 'Starships',
        sub: 'Is death star a starship or a planet?',
    },
]

export default Home
