import React, { useCallback, useEffect, useState } from 'react'
import Layout from 'components/layout/Layout'
import Button from 'components/ui/Button/Button'
import ModalContainer from 'components/ui/Modal/ModalContainer'
import SkeletonCard from 'components/ui/SkeletonCard/SkeletonCard'
import { getVehicleList, VehicleDetail, VehicleList } from 'features/vehicles/vehiclesApi'
import VehicleCard from 'features/vehicles/VehicleCard'
import { GetStaticProps } from 'next'
import { debounce } from 'lodash'
import Search from 'components/ui/Search/Search'

interface VehicleListProps {
    initialData: VehicleList
}

const VehicleList = (props: VehicleListProps) => {
    const [isLoading, setIsLoading] = useState(false)
    const [list, setList] = useState<VehicleDetail[]>([])
    const [pagination, setPagination] = useState({
        page: 1,
        next: false,
        total: 0,
    })

    const [selectedData, setSelectedData] = useState<null | VehicleDetail>(null)
    const [search, setSearch] = useState('')

    useEffect(() => {
        setList(props.initialData.results)
        setPagination({
            page: 1,
            next: props.initialData.next !== null,
            total: props.initialData.count,
        })
    }, [props.initialData])

    const onSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = e.target

        setSearch(value)
        setIsLoading(true)
        onSearch(value)
    }

    const onSearch = useCallback(debounce((search: string) => {
        setList([])
        setPagination(prev => ({
            ...prev,
            total: 0,
        }))
        setIsLoading(true)

        getVehicleList(1, search)
            .then(res => res.data)
            .then(data => {
                setList(data.results)
                setPagination(prev => ({
                    ...prev,
                    page: 1,
                    next: data.next !== null,
                    total: data.count,
                }))
                setIsLoading(false)
            })
            .catch(err => {
                console.error(err)
                setIsLoading(false)
            })
    }, 300), [])

    const clearSearch = () => {
        setSearch('')
        setList(props.initialData.results)
        setPagination({
            page: 1,
            next: props.initialData.next !== null,
            total: props.initialData.count,
        })
    }

    const loadMore = () => {
        const page = pagination.page + 1

        setIsLoading(true)

        getVehicleList(page, search)
            .then(res => res.data)
            .then(data => {
                setList(prev => prev.concat(data.results))
                setPagination(prev => ({
                    ...prev,
                    page,
                    next: data.next !== null,
                    total: data.count,
                }))
                setIsLoading(false)
            })
            .catch(err => {
                console.error(err)
                setIsLoading(false)
            })
    }

    return (
        <>
            <ModalContainer
                isOpen={selectedData !== null}
                onClose={() => setSelectedData(null)}
            >
                <div className="w-full max-w-[450px]">
                    {
                        selectedData ?
                            <VehicleCard {...selectedData} />
                            :
                            <SkeletonCard />
                    }
                </div>
            </ModalContainer>
            <Layout
                meta={{
                    title: "All the Alien Vehicle in Star Wars",
                    description: "There's a lot of alien vehicles in star wars",
                    url: '/vehicles'
                }}
            >
                <div className="py-5 md:py-10">
                    <div className="container px-5 md:px-40">
                        <h1 className='text-xl text-gray-200 font-semibold mb-4 md:mb-5 py-1 pl-3 border-b-2 border-t-2 border-gray-500 uppercase'>
                            The Vehicle
                        </h1>
                        <div className="mb-4 md:mb-5 px-3 flex items-center justify-between">
                            <p className='text-gray-200 font-medium'>
                                Total: {pagination.total}
                            </p>
                            <Search
                                value={search}
                                onChange={onSearchChange}
                                onClear={clearSearch}
                                isLoading={isLoading}
                            />
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-2 2xl:grid-cols-3 gap-5 md:gap-8">
                            {
                                list.map((planets, index) =>
                                    <VehicleCard
                                        {...planets}
                                        key={index}
                                        onClick={() => setSelectedData(planets)}
                                    />
                                )
                            }
                            {
                                isLoading &&
                                <>
                                    <SkeletonCard />
                                    <SkeletonCard />
                                </>
                            }
                            {
                                (list.length === 0 && !isLoading) &&
                                <div className="md:col-span-2 2xl:col-span-3 text-sm text-gray-200 text-center mt-4">
                                    You sure that&apos;s the name?
                                </div>
                            }
                        </div>
                        {
                            (pagination.next && !isLoading) &&
                            <div className="flex items-center justify-center mt-8">
                                <Button onClick={loadMore} disabled={isLoading}>
                                    Load More
                                </Button>
                            </div>
                        }
                    </div>
                </div>
            </Layout>
        </>
    )
}

export const getStaticProps: GetStaticProps = async () => {
    const res = await getVehicleList(1)

    if (res.status !== 200) return {
        redirect: {
            destination: '/',
            permanent: false,
        }
    }

    const data = res.data

    return {
        props: {
            initialData: data,
        }
    }
}

export default VehicleList
