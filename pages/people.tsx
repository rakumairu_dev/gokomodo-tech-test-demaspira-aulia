import React, { useCallback, useEffect, useState } from 'react'
import Layout from 'components/layout/Layout'
import Button from 'components/ui/Button/Button'
import ModalContainer from 'components/ui/Modal/ModalContainer'
import SkeletonCard from 'components/ui/SkeletonCard/SkeletonCard'
import { getPeopleList, PeopleDetail, PeopleList } from 'features/people/peopleApi'
import PeopleCard from 'features/people/PeopleCard'
import { GetStaticProps } from 'next'
import { debounce } from 'lodash'
import Search from 'components/ui/Search/Search'

interface PeopleListProps {
    initialData: PeopleList
}

const PeopleList = (props: PeopleListProps) => {
    const [isLoading, setIsLoading] = useState(false)
    const [list, setList] = useState<PeopleDetail[]>([])
    const [pagination, setPagination] = useState({
        page: 1,
        next: false,
        total: 0,
    })

    const [selectedData, setSelectedData] = useState<null | PeopleDetail>(null)
    const [search, setSearch] = useState('')

    useEffect(() => {
        setList(props.initialData.results)
        setPagination({
            page: 1,
            next: props.initialData.next !== null,
            total: props.initialData.count,
        })
    }, [props.initialData])

    const onSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = e.target

        setSearch(value)
        setIsLoading(true)
        onSearch(value)
    }

    const onSearch = useCallback(debounce((search: string) => {
        setList([])
        setPagination(prev => ({
            ...prev,
            total: 0,
        }))
        setIsLoading(true)

        getPeopleList(1, search)
            .then(res => res.data)
            .then(data => {
                setList(data.results)
                setPagination(prev => ({
                    ...prev,
                    page: 1,
                    next: data.next !== null,
                    total: data.count,
                }))
                setIsLoading(false)
            })
            .catch(err => {
                console.error(err)
                setIsLoading(false)
            })
    }, 300), [])

    const clearSearch = () => {
        setSearch('')
        setList(props.initialData.results)
        setPagination({
            page: 1,
            next: props.initialData.next !== null,
            total: props.initialData.count,
        })
    }

    const loadMore = () => {
        const page = pagination.page + 1

        setIsLoading(true)

        getPeopleList(page, search)
            .then(res => res.data)
            .then(data => {
                setList(prev => prev.concat(data.results))
                setPagination(prev => ({
                    ...prev,
                    page,
                    next: data.next !== null,
                    total: data.count,
                }))
                setIsLoading(false)
            })
            .catch(err => {
                console.error(err)
                setIsLoading(false)
            })
    }

    return (
        <>
            <ModalContainer
                isOpen={selectedData !== null}
                onClose={() => setSelectedData(null)}
            >
                <div className="w-full max-w-[450px]">
                    {
                        selectedData ?
                            <PeopleCard {...selectedData} />
                            :
                            <SkeletonCard />
                    }
                </div>
            </ModalContainer>
            <Layout
                meta={{
                    title: "Meet the Legends!",
                    description: 'You can see everyone in star wars universe here!',
                    url: '/people'
                }}
            >
                <div className="py-5 md:py-10">
                    <div className="container px-5 md:px-40">
                        <h1 className='text-xl text-gray-200 font-semibold mb-4 md:mb-5 py-1 pl-3 border-b-2 border-t-2 border-gray-500 uppercase'>
                            The People
                        </h1>
                        <div className="mb-4 md:mb-5 px-3 flex items-center justify-between">
                            <p className='text-gray-200 font-medium'>
                                Total: {pagination.total}
                            </p>
                            <Search
                                value={search}
                                onChange={onSearchChange}
                                onClear={clearSearch}
                                isLoading={isLoading}
                            />
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-2 2xl:grid-cols-3 gap-5 md:gap-8">
                            {
                                list.map((people, index) =>
                                    <PeopleCard
                                        {...people}
                                        key={index}
                                        onClick={() => setSelectedData(people)}
                                    />
                                )
                            }
                            {
                                isLoading &&
                                <>
                                    <SkeletonCard />
                                    <SkeletonCard />
                                </>
                            }
                            {
                                (list.length === 0 && !isLoading) &&
                                <div className="md:col-span-2 2xl:col-span-3 text-sm text-gray-200 text-center mt-4">
                                    You sure that&apos;s their name?
                                </div>
                            }
                        </div>
                        {
                            (pagination.next && !isLoading) &&
                            <div className="flex items-center justify-center mt-8">
                                <Button onClick={loadMore} disabled={isLoading}>
                                    Load More
                                </Button>
                            </div>
                        }
                    </div>
                </div>
            </Layout>
        </>
    )
}

export const getStaticProps: GetStaticProps = async () => {
    const res = await getPeopleList(1)

    if (res.status !== 200) return {
        redirect: {
            destination: '/',
            permanent: false,
        }
    }

    const data = res.data

    return {
        props: {
            initialData: data,
        }
    }
}

export default PeopleList
