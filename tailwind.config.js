module.exports = {
    content: [
        './pages/**/*.tsx',
        './components/**/*.tsx',
        './features/**/*.tsx',
    ],
    darkMode: 'media', // or 'media' or 'class'
    theme: {
        container: {
            center: true,
        },
        extend: {
            colors: {
                primary: '#9e4f60',
                'primary-dark': '#763745',
            },
            boxShadow: {
                primary: '3px 3px 7px -1px rgba(158,79,96,0.72)',
            }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
