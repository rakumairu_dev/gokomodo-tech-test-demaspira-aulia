# Gokomodo - Technical Test - Demaspira Aulia

This project is for Gokomodo Technical Test: Create an application to consume Star Wars API

Author: Demaspira Aulia

Check the [live demo here](https://gokomodo-tech-test-demaspira-aulia.vercel.app/)

## Prequisites

You will need Node.js and NPM to use this project.

You windows user, you can check for the installation file [here](https://nodejs.org/).

And for linux user, you can check for the installation file [here](https://nodejs.org/) or search for your spesific distro in the web.

## Getting Started

First, install all required dependecies:
```
npm install
```

start the development server:
```
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser.

## Developing

You can start developing by editing the index.tsx in pages folder (pages/index.tsx).

## Build and Serve

You can build and serve the project for production by using:
```
npm run build
npm run start
```

## Learn More

You can learn more about Next.js, and Tailwindcss here:

- [Next.js](https://nextjs.org/docs/getting-started)
- [Tailwindcss](https://tailwindcss.com/docs/installation)