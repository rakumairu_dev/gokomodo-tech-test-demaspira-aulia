export interface BasicApiListRes {
    count: number
    next: string | null
    previous: string | null
}