import Axios from 'axios'

const api = Axios.create({
    baseURL: 'https://swapi.dev',
})

export default api
