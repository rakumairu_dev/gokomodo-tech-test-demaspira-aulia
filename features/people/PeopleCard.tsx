import React from 'react'

import { PeopleDetail } from './peopleApi'
import { BsGenderAmbiguous } from 'react-icons/bs'
import { capitalize } from 'utils/util'
import { FaBirthdayCake } from 'react-icons/fa'
import { TbArrowAutofitHeight } from 'react-icons/tb'
import { MdMonitorWeight } from 'react-icons/md'
import { RiEye2Fill } from 'react-icons/ri'
import { GiHairStrands } from 'react-icons/gi'
import { AiFillSkin } from 'react-icons/ai'

interface PeopleCardProps {
    onClick?: () => void
}

const PeopleCard = (props: PeopleDetail & PeopleCardProps) => {
    return (
        <div onClick={props.onClick} className={`rounded-lg px-3 py-4 shadow-lg transform-transition scale-100 w-full bg-zinc-700 text-white ${props.onClick ? 'cursor-pointer lg:hover:scale-105' : ''}`}>
            <p className='text-xl font-bold pb-3 border-b-2 border-primary mb-4'>
                {props.name}
            </p>
            <div className="grid grid-cols-3 gap-3">
                <div className="flex items-center">
                    <FaBirthdayCake color='white' size={18} />
                    <p className='font-medium ml-2'>
                        {capitalize(props.birth_year)}
                    </p>
                </div>
                <div className="flex items-center">
                    <BsGenderAmbiguous color='white' size={18} />
                    <p className='font-medium ml-2'>
                        {capitalize(props.gender)}
                    </p>
                </div>
                <div className="flex items-center">
                    <TbArrowAutofitHeight color='white' size={18} />
                    <p className='font-medium ml-2'>
                        {capitalize(props.height.toString())}
                    </p>
                </div>
                <div className="flex items-center">
                    <MdMonitorWeight color='white' size={18} />
                    <p className='font-medium ml-2'>
                        {capitalize(props.mass.toString())}
                    </p>
                </div>
                <div className="flex items-center">
                    <RiEye2Fill color='white' size={18} />
                    <p className='font-medium ml-2'>
                        {capitalize(props.eye_color)}
                    </p>
                </div>
                <div className="flex items-center">
                    <GiHairStrands color='white' size={18} />
                    <p className='font-medium ml-2'>
                        {capitalize(props.hair_color)}
                    </p>
                </div>
                <div className="flex items-center">
                    <AiFillSkin color='white' size={18} />
                    <p className='font-medium ml-2'>
                        {capitalize(props.skin_color)}
                    </p>
                </div>
            </div>
        </div>
    )
}

export default PeopleCard
