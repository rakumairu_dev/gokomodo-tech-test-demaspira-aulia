import api from "lib/api";
import { BasicApiListRes } from "utils/types";

export interface PeopleDetail {
    name: string
    height: number
    mass: number
    hair_color: string
    skin_color: string
    eye_color: string
    birth_year: string
    gender: string
    homeworld: string
    films: string[]
    species: string[]
    vehicles: string[]
    starships: string[]
    created: string
    edited: string
    url: string
}

export interface PeopleList extends BasicApiListRes {
    results: PeopleDetail[]
}

export const getPeopleList = (page: number, search?: string) => {
    return api.get<PeopleList>(`/api/people`, {params: {
        page,
        search,
    }})
}