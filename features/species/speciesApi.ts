import api from "lib/api";
import { BasicApiListRes } from "utils/types";

export interface SpeciesDetail {
    name: string
    classification: string
    designation: string
    average_height: string
    skin_colors: string
    hair_colors: string
    eye_colors: string
    average_lifespan: string
    homeworld: string
    language: string
    people: string[]
    films: string[]
    created: string
    edited: string
    url: string
}

export interface SpeciesList extends BasicApiListRes {
    results: SpeciesDetail[]
}

export const getSpeciesList = (page: number, search?: string) => {
    return api.get<SpeciesList>(`/api/species`, {params: {
        page,
        search,
    }})
}