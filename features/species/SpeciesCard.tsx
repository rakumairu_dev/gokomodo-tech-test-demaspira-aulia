import React from 'react'

import { SpeciesDetail } from './speciesApi'
import { capitalize, convertNumber, slugToText } from 'utils/util'

interface SpeciesCardProps {
    onClick?: () => void
}

const SpeciesCard = (props: SpeciesDetail & SpeciesCardProps) => {
    const preText = (text: string, isNumber?: boolean) => {
        if (['n/a', 'unknown'].includes(text)) return text

        if (isNumber) {
            return convertNumber(text)
        } else {
            return capitalize(text)
        }
    }

    return (
        <div onClick={props.onClick} className={`rounded-lg px-3 py-4 shadow-lg transform-transition scale-100 w-full bg-zinc-700 text-white ${props.onClick ? 'cursor-pointer md:hover:scale-105' : ''}`}>
            <p className='text-xl font-bold pb-3 border-b-2 border-primary mb-4'>
                {props.name}
            </p>
            <div className="grid grid-cols-1 gap-3">
                <div className="flex items-start">
                    <p>
                        {slugToText('classification')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.classification)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('designation')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.designation)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('average_height')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.average_height)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('skin_colors')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.skin_colors)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('hair_colors')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.hair_colors)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('eye_colors')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.eye_colors)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('average_lifespan')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.average_lifespan)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('language')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.language)}
                    </p>
                </div>
            </div>
        </div>
    )
}

export default SpeciesCard
