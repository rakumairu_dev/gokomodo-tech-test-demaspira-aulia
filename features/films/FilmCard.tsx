import React from 'react'

import { FilmDetail } from './filmsApi'
import { capitalize, convertNumber, slugToText } from 'utils/util'

interface FilmCardProps {
    onClick?: () => void
}

const FilmCard = (props: FilmDetail & FilmCardProps) => {
    const preText = (text: string, isNumber?: boolean) => {
        if (text === 'unknown') return text

        if (isNumber) {
            return convertNumber(text)
        } else {
            return capitalize(text)
        }
    }

    return (
        <div onClick={props.onClick} className={`rounded-lg px-3 py-4 shadow-lg transform-transition scale-100 w-full bg-zinc-700 text-white ${props.onClick ? 'cursor-pointer md:hover:scale-105' : ''}`}>
            <p className='text-xl font-bold pb-3 border-b-2 border-primary mb-4'>
                {props.title}
            </p>
            <div className="grid grid-cols-1 gap-3">
                <div className="flex items-start">
                    <p>
                        {slugToText('episode')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.episode_id.toString())}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('director')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.director)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('producer')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.producer)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('release_date')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.release_date)}
                    </p>
                </div>
                <p className='text-sm text-gray-200'>
                    {props.opening_crawl}
                </p>
            </div>
        </div>
    )
}

export default FilmCard
