import api from "lib/api";
import { BasicApiListRes } from "utils/types";

export interface FilmDetail {
    title: string
    episode_id: number
    opening_crawl: string
    director: string
    producer: string
    release_date: string
    characters: string[]
    planets: string[]
    starships: string[]
    vehicles: string[]
    species: string[]
    created: string
    edited: string
    url: string
}

export interface FilmList extends BasicApiListRes {
    results: FilmDetail[]
}

export const getFilmList = (page: number, search?: string) => {
    return api.get<FilmList>(`/api/films`, {params: {
        page,
        search,
    }})
}