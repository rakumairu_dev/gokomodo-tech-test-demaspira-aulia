import api from "lib/api";
import { BasicApiListRes } from "utils/types";

export interface StarShipDetail {
    name: string
    model: string
    manufacturer: string
    cost_in_credits: string
    length: string
    max_atmosphering_speed: string
    crew: string
    passengers: string
    cargo_capacity: string
    consumables: string
    hyperdrive_rating: string
    MGLT: string
    starship_class: string
    pilots: string[]
    films: string[]
    created: string
    edited: string
    url: string
}

export interface StarShipList extends BasicApiListRes {
    results: StarShipDetail[]
}

export const getStarShipList = (page: number, search?: string) => {
    return api.get<StarShipList>(`/api/starships`, {params: {
        page,
        search,
    }})
}