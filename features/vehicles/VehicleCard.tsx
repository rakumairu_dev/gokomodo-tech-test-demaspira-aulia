import React from 'react'

import { VehicleDetail } from './vehiclesApi'
import { capitalize, convertNumber, slugToText } from 'utils/util'

interface VehicleCardProps {
    onClick?: () => void
}

const VehicleCard = (props: VehicleDetail & VehicleCardProps) => {
    const preText = (text: string, isNumber?: boolean) => {
        if (['n/a', 'unknown'].includes(text)) return text

        if (isNumber) {
            return convertNumber(text)
        } else {
            return capitalize(text)
        }
    }

    return (
        <div onClick={props.onClick} className={`rounded-lg px-3 py-4 shadow-lg transform-transition scale-100 w-full bg-zinc-700 text-white ${props.onClick ? 'cursor-pointer md:hover:scale-105' : ''}`}>
            <p className='text-xl font-bold pb-3 border-b-2 border-primary mb-4'>
                {props.name}
            </p>
            <div className="grid grid-cols-1 gap-3">
                <div className="flex items-start">
                    <p>
                        {slugToText('model')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.model)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('manufacturer')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.manufacturer)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('cost_in_credits')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.cost_in_credits, true)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('length')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.length)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('max_atmosphering_speed')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.max_atmosphering_speed)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('crew')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.crew)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('passengers')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.passengers)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('cargo_capacity')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.cargo_capacity, true)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('consumables')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.consumables)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('vehicle_class')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.vehicle_class)}
                    </p>
                </div>
            </div>
        </div>
    )
}

export default VehicleCard
