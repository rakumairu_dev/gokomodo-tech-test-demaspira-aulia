import api from "lib/api";
import { BasicApiListRes } from "utils/types";

export interface VehicleDetail {
    name: string
    model: string
    manufacturer: string
    cost_in_credits: string
    length: string
    max_atmosphering_speed: string
    crew: string
    passengers: string
    cargo_capacity: string
    consumables: string
    vehicle_class: string
    pilots: string[]
    films: string[]
    created: string
    edited: string
    url: string
}

export interface VehicleList extends BasicApiListRes {
    results: VehicleDetail[]
}

export const getVehicleList = (page: number, search?: string) => {
    return api.get<VehicleList>(`/api/vehicles`, {params: {
        page,
        search,
    }})
}