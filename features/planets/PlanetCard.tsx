import React from 'react'

import { PlanetDetail } from './planetsApi'
import { capitalize, convertNumber, slugToText } from 'utils/util'

interface PlanetCardProps {
    onClick?: () => void
}

const PlanetCard = (props: PlanetDetail & PlanetCardProps) => {
    const preText = (text: string, isNumber?: boolean) => {
        if (text === 'unknown') return text

        if (isNumber) {
            return convertNumber(text)
        } else {
            return capitalize(text)
        }
    }

    return (
        <div onClick={props.onClick} className={`rounded-lg px-3 py-4 shadow-lg transform-transition scale-100 w-full bg-zinc-700 text-white ${props.onClick ? 'cursor-pointer md:hover:scale-105' : ''}`}>
            <p className='text-xl font-bold pb-3 border-b-2 border-primary mb-4'>
                {props.name}
            </p>
            <div className="grid grid-cols-1 gap-3">
                <div className="flex items-start">
                    <p>
                        {slugToText('rotation_period')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.rotation_period, true)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('orbital_period')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.orbital_period, true)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('diameter')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.diameter, true)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('climate')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.climate || '-')}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('gravity')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.gravity)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('terrain')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.terrain)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('surface_water')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.surface_water)}
                    </p>
                </div>
                <div className="flex items-start">
                    <p>
                        {slugToText('population')}: 
                    </p>
                    <p className='font-medium ml-2'>
                        {preText(props.population, true)}
                    </p>
                </div>
            </div>
        </div>
    )
}

export default PlanetCard
