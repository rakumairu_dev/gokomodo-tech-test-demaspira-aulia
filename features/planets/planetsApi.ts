import api from "lib/api";
import { BasicApiListRes } from "utils/types";

export interface PlanetDetail {
    name: string
    rotation_period: string
    orbital_period: string
    diameter: string
    climate: string
    gravity: string
    terrain: string
    surface_water: string
    population: string
    residents: string[]
    films: string[]
    created: string
    edited: string
    url: string
}

export interface PlanetList extends BasicApiListRes {
    results: PlanetDetail[]
}

export const getPlanetList = (page: number, search?: string) => {
    return api.get<PlanetList>(`/api/planets`, {params: {
        page,
        search,
    }})
}